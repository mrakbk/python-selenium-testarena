from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


def is_element_present(browser, css_selector_text):
    try:
        browser.find_element(By.CSS_SELECTOR, css_selector_text)
        return True
    except NoSuchElementException:
        return False


def verify_has_element(browser, selector, expected_result):
    assert is_element_present(browser, selector) is expected_result


def wait_for_element(browser, css_selector):
    wait = WebDriverWait(browser, 10)
    selected_element = (By.CSS_SELECTOR, css_selector)
    wait.until(expected_conditions.element_to_be_clickable(selected_element))
