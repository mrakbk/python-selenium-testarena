# UI Testing with Selenium

The repository contains the Test Arena UI tests written in ``Python`` and ``Selenium`` following the Page Object Pattern (POP) concept.

## Steps
The general test steps are:

* Login to TestArena;
* Open the admin panel;
* Add a new project;
* Go to the Projects subpage;
* Find the newly created project by name.


## File
Path to the main test file is [tests/test_arena_projects.py](tests/test_arena_projects.py)
