from selenium.webdriver.common.by import By
from utils.selenium_utils import verify_has_element, wait_for_element


class ArenaProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_admin(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin').click()

    def click_new_project(self):
        wait_for_element(self.browser, '.button_link_li:first-child')
        self.browser.find_element(By.CSS_SELECTOR, '.button_link_li:first-child').click()

    def click_projects_list(self):
        self.browser.find_element(By.CSS_SELECTOR, '.item2').click()

    def search_project(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_project_present_on_list(self, project_name):
        assert self.browser.find_element(By.CSS_SELECTOR, 'tr td:first-child a').text == project_name

    def verify_list_is_not_empty(self):
        verify_has_element(self.browser, '.empty-text', False)
