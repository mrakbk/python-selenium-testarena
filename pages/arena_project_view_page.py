from selenium.webdriver.common.by import By
from utils.selenium_utils import verify_has_element, wait_for_element


class ArenaProjectViewPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_project_is_created(self):
        wait_for_element(self.browser, '#j_info_box')
        verify_has_element(self.browser, '#j_info_box', True)

    def click_projects(self):
        self.browser.find_element(By.CSS_SELECTOR, '.item2').click()
