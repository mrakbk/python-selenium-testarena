from utils.random_message import get_random_string
from utils.selenium_utils import is_element_present
from selenium.webdriver.common.by import By


class ArenaAddProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def get_unique_input_text(self, element_name, text_length, number_of_tries=3):

        """
        This function generates a random string of a given length and checks that it has not already been used as
        a prefix or project name when creating a new project.

        :param element_name: indicate for which element we want to generate a string: 'prefix' or 'name'.
        :param text_length: length of generated string.
        :param number_of_tries: number of attempts to generate a unique prefix or project name (default 3).
        :return: returns a string of the given length or empty (if it failed to generate a unique prefix or
        name for the given number of attempts).
        """

        for i in range(number_of_tries - 1):
            random_text = get_random_string(text_length)
            self.browser.find_element(By.CSS_SELECTOR, '#' + element_name).send_keys(random_text)
            self.browser.find_element(By.CSS_SELECTOR, 'input#save').click()

            if is_element_present(self.browser, 'label[for="' + element_name + '"] + .error_msg'):
                self.browser.find_element(By.CSS_SELECTOR, '#' + element_name).clear()
            else:
                self.browser.find_element(By.CSS_SELECTOR, '#' + element_name).clear()
                return random_text
        return ''

    def fill_form(self, project_name, project_prefix):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(project_prefix)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(get_random_string(40))

    def send_form(self):
        self.browser.find_element(By.CSS_SELECTOR, 'input#save').click()
