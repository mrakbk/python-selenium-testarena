import pytest
from selenium.webdriver import Chrome

from pages.arena_add_project_page import ArenaAddProjectPage
from pages.arena_home_page import ArenaHomePage
from pages.arena_login_page import ArenaLoginPage
from pages.arena_projects_page import ArenaProjectsPage
from pages.arena_project_view_page import ArenaProjectViewPage


@pytest.fixture()
def browser():
    browser = Chrome()
    browser.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(browser)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_should_open_projects_page(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectsPage(browser)
    arena_project_page.verify_title('Projekty')


def test_should_add_new_project(browser):
    # Go to the administration page, then create new project
    arena_projects_page = ArenaProjectsPage(browser)
    arena_projects_page.click_admin()
    arena_projects_page.click_new_project()

    # Find unique project and prefix name
    arena_add_project_page = ArenaAddProjectPage(browser)
    project_name = arena_add_project_page.get_unique_input_text('name', 10, 3)
    project_prefix = arena_add_project_page.get_unique_input_text('prefix', 5, 3)

    # Check if unique texts for project_name and project_prefix are generated
    assert len(project_name) > 0
    assert len(project_prefix) > 0

    # Fill and send a form
    arena_add_project_page.fill_form(project_name, project_prefix)
    arena_add_project_page.send_form()

    # Check that a new project has been created
    arena_added_project_view = ArenaProjectViewPage(browser)
    arena_added_project_view.verify_project_is_created()

    # Go to the projects subpage
    arena_added_project_view.click_projects()

    # Find created project
    arena_projects_page.search_project(project_name)
    arena_projects_page.verify_list_is_not_empty()
    arena_projects_page.verify_project_present_on_list(project_name)
