import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from utils.random_message import generate_random_text


@pytest.fixture(scope='module')
def browser():
    browser = Chrome()
    browser.get('http://demo.testarena.pl/zaloguj')
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys('administrator@testarena.pl')
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')
    browser.find_element(By.CSS_SELECTOR, '#login').click()
    browser.find_element(By.CSS_SELECTOR, '.top_messages').click()
    is_element_present = EC.element_to_be_clickable((By.CSS_SELECTOR, '#j_msgContent'))
    WebDriverWait(browser, 10).until(is_element_present)
    yield browser
    browser.quit()



def test_should_add_new_message(browser):
    random_text = generate_random_text(10)
    browser.find_element(By.CSS_SELECTOR, '.top_messages').click()
    is_element_present = EC.element_to_be_clickable((By.CSS_SELECTOR, '#j_msgContent'))
    WebDriverWait(browser, 10).until(is_element_present)