import pytest
from selenium.webdriver import Chrome
from pages.arena_home_page import ArenaHomePage
from pages.arena_login_page import ArenaLoginPage
from pages.arena_message_page import ArenaMessagePage
from pages.arena_projects_page import ArenaProjectsPage


@pytest.fixture
def browser():
    browser = Chrome()
    browser.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(browser)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_should_display_email_in_user_section(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.check_email('administrator@testarena.pl')


def test_should_successfully_logout(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_logout()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_should_open_messages_and_display_text_area(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_mail()
    arena_message_page = ArenaMessagePage(browser)
    arena_message_page.wait_for_text_area_load()


def test_should_open_projects_page(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectsPage(browser)
    arena_project_page.verify_title('Projekty')
